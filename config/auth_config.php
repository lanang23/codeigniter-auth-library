<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['auth_config'] = array(
	'redirect_unauthorized' => 'auth/login',
	'redirect_logout' => 'auth/login',
	'table' => 'users',
	'fields' => array(
		'id' => 'id', // misalkan field yang digunakan id_user, maka diganti menjadi => 'id_user'
		'username' => 'username', // misalkan field yang digunakan email, maka diganti menjadi => 'email'
		'password' => 'password', // misalkan field yang digunakan hashed_password, maka diganti menjadi => 'hashed_password'
	),
	/*
	'where' => array(
		'active' => 1
	), // ini diisi jika ada kondisi khusus untuk bisa login, misalkan statusnya harus active
	*/ 
	'hash' => 'sha1', // md5, sha1, sha256, atau dikosongi jika tanpa hash
	'result_fields' => array('id','username','fullname','email','groups_id'), 
);
