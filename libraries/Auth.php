<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth {
	private $ci;
	private $auth_config;
	private $config;
	
	public function __construct($config = array())
	{
		$this->ci =& get_instance();
		
		$this->config = $config;
		
		if (FALSE === isset($this->ci->session))
		{
			$this->ci->load->library('session');
		}
		
		if (FALSE === function_exists('redirect'))
		{
			$this->ci->load->helper('url');
		}
		
		$this->ci->load->config('auth_config', FALSE, TRUE);
		$config_item = 'auth_config';
		
		if (isset($config['config_item']) && !empty($config['config_item']))
		{
			$config_item = $config['config_item'];
		}
		
		$this->auth_config = $this->ci->config->item($config_item);
				
		if (empty($this->auth_config))
		{
			show_error($config_item.' belum diset!');
		}
		
		$this->table = $this->auth_config['table'];
	}
	
	public function is_authorized($auto_redirect = FALSE)
	{
		$redirect = TRUE;
		
		if (TRUE === $this->ci->session->userdata('IS_AUTHORIZED'))
		{
			$redirect = FALSE;
			
			if (FALSE === $auto_redirect)
			{
				return true;
			}
		}
		
		if (FALSE === $auto_redirect)
		{
			return false;
		}
		else if (TRUE === $redirect)
		{
			redirect($this->auth_config['redirect_unauthorized']);
		}
	}
	
	public function login($username, $password, $where=array())
	{
		if (FALSE === isset($this->ci->db))
		{
			show_error('database harus diload terlebih dahulu!');
		}
		
		switch ($this->auth_config['hash'])
		{
			case 'md5':
				$password = md5($password);
			break;
			
			case 'sha1':
				$password = sha1($password);
			break;
			
			case 'sha256':
				$password = sha256($password);
			break;
		}
		
		$this->ci->db->select(implode(',', $this->auth_config['result_fields']));
		
		$this->ci->db->where(array(
			$this->auth_config['fields']['username'] => $username, 
			$this->auth_config['fields']['password'] => $password,
		));
		
		if (isset($this->auth_config['where']) && is_array($this->auth_config['where']))
		{
			$this->ci->db->where($this->auth_config['where']);
		}
		
		if (is_array($where) && count($where) > 0)
		{
			$this->ci->db->where($where);
		}
		
		$get = $this->ci->db->get($this->table);
		
		if ($row = $get->first_row('array'))
		{
			if (FALSE === isset($this->config['auto_set_session']) || $this->config['auto_set_session'] === TRUE)
			{
				if (isset($row[$this->auth_config['fields']['id']]))
				{
					$row['id'] = $row[$this->auth_config['fields']['id']];
				}
				
				$this->ci->session->set_userdata($row);
				$this->ci->session->set_userdata('IS_AUTHORIZED', TRUE);
			}
			
			return $row;
		}
		
		return false;
	}
	
	public function logout()
	{
		$this->ci->session->sess_destroy();
		
		redirect($this->auth_config['redirect_logout']);
	}
	
	public function set_config($key, $value)
	{
		$this->config[$key] = $value;
	}
}
